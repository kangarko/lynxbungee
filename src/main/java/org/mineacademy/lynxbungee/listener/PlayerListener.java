package org.mineacademy.lynxbungee.listener;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import org.mineacademy.bfo.Common;
import org.mineacademy.bfo.annotation.AutoRegister;

@AutoRegister
public final class PlayerListener implements Listener {

	@EventHandler
	public void onServerSwitch(final ServerSwitchEvent event) {
		final ProxiedPlayer player = event.getPlayer();

		if (event.getFrom() == null)
			Common.log(player.getName() + " just connected to " + player.getServer().getInfo().getName());
		else
			Common.log(player.getName() + " switched from " + event.getFrom().getName()
					+ " to " + player.getServer().getInfo().getName());
	}
}
