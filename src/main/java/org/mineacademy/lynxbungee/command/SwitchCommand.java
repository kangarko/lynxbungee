package org.mineacademy.lynxbungee.command;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.mineacademy.bfo.Common;
import org.mineacademy.bfo.annotation.AutoRegister;
import org.mineacademy.bfo.command.SimpleCommand;

import java.util.List;

@AutoRegister
public final class SwitchCommand extends SimpleCommand {

	public SwitchCommand() {
		super("switch");

		setMinArguments(2);
		setUsage("<player> <server>");
	}

	@Override
	protected void onCommand() {
		final ProxiedPlayer player = findPlayer(args[0]);
		final ServerInfo server = ProxyServer.getInstance().getServerInfo(args[1]);

		checkNotNull(server, "No such server '{1}'. Available: " + Common.getServerNames());

		player.connect(server);
	}

	@Override
	protected List<String> tabComplete() {

		switch (args.length) {
			case 1:
				return completeLastWordPlayerNames();

			case 2:
				return completeLastWordServerNames();
		}

		return NO_COMPLETE;
	}
}
