package org.mineacademy.lynxbungee.command;

import java.util.List;

import org.mineacademy.bfo.Common;
import org.mineacademy.bfo.annotation.AutoRegister;
import org.mineacademy.bfo.command.SimpleCommand;
import org.mineacademy.bfo.model.BukkitRunnable;
import org.mineacademy.bfo.model.SimpleScoreboard;
import org.mineacademy.bfo.model.Variables;
import org.mineacademy.bfo.remain.Remain;

import net.md_5.bungee.api.connection.ProxiedPlayer;

@AutoRegister
public final class FancyCommand extends SimpleCommand {

	public FancyCommand() {
		super("fancy");

		setMinArguments(1);
		setUsage("<title/actionbar/tablist/scoreboard>");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final String param = args[0].toLowerCase();
		final ProxiedPlayer player = getPlayer();

		if ("title".equals(param)) {
			for (final ProxiedPlayer online : Remain.getOnlinePlayers())
				Remain.sendTitle(online, "&6Hello &e{player_name}!", "&cWelcome to the {server_name}");

		} else if ("actionbar".equals(param)) {
			Remain.sendActionBar(player, "&cMy &6custom &emessage");

		} else if ("tablist".equals(param)) {
			Remain.sendTablist(player, "&6[!] Welcome " + player.getName(), "\nVisit mineacademy.org\nCurrent server: {server_name}");

		} else if ("scoreboard".equals(param)) {
			final SimpleScoreboard scoreboard = new SimpleScoreboard() {
				@Override
				protected String replaceVariables(final ProxiedPlayer player, final String text) {
					return Variables.replace(text, player);
				}
			};

			scoreboard.setTitle("&6Lynx&eBungee");
			scoreboard.setLines("&cFirst line", "&dSecond line", "#cc44ffServer: {server_name}");

			Common.runTimerAsync(10, new BukkitRunnable() {
				@Override
				public void run() {
					if (!player.isConnected()) {
						cancel();

						return;
					}

					scoreboard.send(player);
				}
			});
		}

	}

	@Override
	protected List<String> tabComplete() {
		return args.length == 1 ? completeLastWord("title", "actionbar", "tablist", "scoreboard") : NO_COMPLETE;
	}
}
