package org.mineacademy.lynxbungee.model;

import java.util.concurrent.TimeUnit;

import org.mineacademy.bfo.annotation.AutoRegister;
import org.mineacademy.bfo.bungee.BungeeListener;
import org.mineacademy.bfo.bungee.message.IncomingMessage;
import org.mineacademy.bfo.bungee.message.OutgoingMessage;
import org.mineacademy.bfo.collection.SerializedMap;
import org.mineacademy.lynxbungee.LynxBungeePlugin;

import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.Connection;

@AutoRegister
public final class LynxBungeeListener extends BungeeListener {

	@Getter
	private static final LynxBungeeListener instance = new LynxBungeeListener();

	/**
	 * The data that are being synced between servers
	 */
	private final SerializedMap serverData = new SerializedMap();

	private LynxBungeeListener() {
		super("plugin:lynx", LynxMessage.class);
	}

	/**
	 * Reschedule data sync task
	 */
	public void scheduleSyncTask() {

		ProxyServer.getInstance().getScheduler().schedule(LynxBungeePlugin.getInstance(), () -> {

			// Upload downstream data here and redistribute over network
			synchronized (this.serverData) {
				final OutgoingMessage message = new OutgoingMessage(LynxMessage.DATA_SYNC);

				for (final ServerInfo server : ProxyServer.getInstance().getServers().values()) {
					if (!this.serverData.containsKey(server.getName())) {
						this.serverData.put(server.getName(), new SerializedMap());

						break;
					}
				}

				message.writeMap(this.serverData);
				message.broadcast();

				this.serverData.clear();
			}

		}, 1, 1, TimeUnit.SECONDS);
	}

	@Override
	public void onMessageReceived(final Connection connection, final IncomingMessage message) {
		final String incomingServerName = message.getServerName();
		final LynxMessage action = (LynxMessage) message.getAction();

		if (action == LynxMessage.CHAT_MESSAGE) {

			for (final ServerInfo server : ProxyServer.getInstance().getServers().values()) {
				if (server.getPlayers().isEmpty() || incomingServerName.equals(server.getName()))
					continue;

				message.forward(server);
			}

			// Or just use our own shortcut method
			//message.forwardToOthers();

		} else if (action == LynxMessage.DATA_SYNC) {
			synchronized (this.serverData) {
				final SerializedMap incomingMap = message.readMap();

				this.serverData.mergeFrom(incomingMap);
			}
		}
	}
}