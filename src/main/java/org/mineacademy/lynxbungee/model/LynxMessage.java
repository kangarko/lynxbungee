package org.mineacademy.lynxbungee.model;

import lombok.Getter;
import org.mineacademy.bfo.bungee.BungeeMessageType;
import org.mineacademy.bfo.collection.SerializedMap;

public enum LynxMessage implements BungeeMessageType {

	CHAT_MESSAGE(
			String.class, // senders name
			String.class // message
	),

	DATA_SYNC(
			SerializedMap.class // data as map<server name, another map of server data (such as players online etc.)>
	);

	//PLAY_SOUND(UUID.class, String.class, Float.class, Float.class);

	@Getter
	private final Class<?>[] content;

	LynxMessage(final Class<?>... content) {
		this.content = content;
	}
}
