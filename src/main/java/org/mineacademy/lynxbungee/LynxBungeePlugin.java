package org.mineacademy.lynxbungee;

import org.mineacademy.bfo.Common;
import org.mineacademy.bfo.model.Variables;
import org.mineacademy.bfo.plugin.SimplePlugin;
import org.mineacademy.lynxbungee.model.LynxBungeeListener;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public class LynxBungeePlugin extends SimplePlugin {

	@Override
	protected void onPluginStart() {
		Common.log("LynxBungee custom plugin was enabled, hurray!");

		Variables.addVariable("server_name", sender -> sender instanceof ProxiedPlayer ? ((ProxiedPlayer) sender).getServer().getInfo().getName() : "");
	}

	@Override
	protected void onReloadablesStart() {
		LynxBungeeListener.getInstance().scheduleSyncTask();
	}

	@Override
	protected void onPluginReload() {
		//registerEvents(new PlayerListener());
	}
}
